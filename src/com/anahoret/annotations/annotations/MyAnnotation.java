package com.anahoret.annotations.annotations;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(MyAnnotationsContainer.class)
@Inherited
@Documented
public @interface MyAnnotation {

    String value();

}
