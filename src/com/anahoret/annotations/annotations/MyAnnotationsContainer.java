package com.anahoret.annotations.annotations;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@interface MyAnnotationsContainer {

    MyAnnotation[] value();

}
