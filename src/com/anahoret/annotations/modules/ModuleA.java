package com.anahoret.annotations.modules;

import com.anahoret.annotations.annotations.Autowired;
import com.anahoret.annotations.annotations.Component;

@Component
public class ModuleA {

    @Autowired
    private ModuleC moduleC;

    @Override
    public String toString() {
        return "(Module A has " + moduleC + ")";
    }

}
