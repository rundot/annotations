package com.anahoret.annotations.modules;

import com.anahoret.annotations.annotations.Autowired;
import com.anahoret.annotations.annotations.Component;

@Component
public class ModuleB {

    @Autowired
    private ModuleA moduleA;

    @Autowired
    private ModuleC moduleC;

    @Override
    public String toString() {
        return "(Module B has " + moduleA + " and " + moduleC + ")";
    }
}
