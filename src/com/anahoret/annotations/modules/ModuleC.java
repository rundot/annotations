package com.anahoret.annotations.modules;

import com.anahoret.annotations.annotations.Component;

@Component
public class ModuleC {

    @Override
    public String toString() {
        return "(Module C has no modules)";
    }
}
