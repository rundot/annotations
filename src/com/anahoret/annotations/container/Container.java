package com.anahoret.annotations.container;

import com.anahoret.annotations.annotations.Autowired;
import com.anahoret.annotations.annotations.Component;
import com.anahoret.annotations.annotations.Singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class Container {

    private static Map<Class<?>, Object> singletons = new HashMap<>();

    public static Object getBean(Class<?> clazz) throws Exception {

        if (clazz.getAnnotation(Component.class) == null) throw new Exception();

        if (clazz.getAnnotation(Singleton.class) != null) {
            return singletons.get(clazz) != null ? singletons.get(clazz) : singletons.put(clazz, clazz.newInstance());
        }

        try {
            Constructor constructor = clazz.getConstructor();
            Object instance = constructor.newInstance();

            Field[] fields = clazz.getDeclaredFields();
            Method[] methods = clazz.getDeclaredMethods();

            for (Field field: fields) {
                Autowired autowired = field.getAnnotation(Autowired.class);
                if (autowired != null && !Arrays.asList(methods).contains("set"+field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1))) {
                    Object fieldValue = getBean(field.getType());
                    boolean oldAccesible = field.isAccessible();
                    field.setAccessible(true);
                    field.set(instance, fieldValue);
                    field.setAccessible(oldAccesible);
                }
            }

            for (Method method : methods) {
                Autowired autowired = method.getAnnotation(Autowired.class);
                String methodName = method.getName();
                if (methodName.matches("^set.+") && autowired != null) {
                    Class<?>[] paramsTypes = method.getParameterTypes();
                    if (paramsTypes.length != 1) throw new IllegalArgumentException();
                    Object paramValue = getBean(paramsTypes[0]);
                    boolean oldAccessible = method.isAccessible();
                    method.setAccessible(true);
                    method.invoke(instance, paramValue);
                    method.setAccessible(oldAccessible);
                }
            }

            return instance;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
